# Fluent - OpenSearch docker image

Image made to serve as a bridge between Fluentd and OpenSearch.

Its main purpose is to capture logs and forward them to OpenSearch using
[Fluent::Plugin::OpenSearch](https://github.com/fluent/fluent-plugin-opensearch).
