#!/bin/bash

set -e

log() {
    printf '%s %s: {"message": "%s"}\n' "$(date +'%Y-%m-%d %H:%M:%S.%N %z')" "entrypoint" "${1}"
}

signal() {
  log "Received SIG${1} signal"
  # shellcheck disable=SC2154
  log "Shutting down supervisord (PID=$supervisord_pid)"
  kill -s "${1}" "$supervisord_pid"
  wait "$supervisord_pid"

  log "Shutting down fluentd (PID=$fluentd_pid)"
  kill -s "${1}" "$fluentd_pid"
  wait "$fluentd_pid"
}

trap "signal 'INT'" SIGINT
trap "signal 'TERM'" SIGTERM
#traps are not supported for the KILL and STOP signals

#Make sure log directories exist
mkdir -p /var/log/fluentd

log "Starting fluentd"
/usr/bin/fluentd \
  -c '/etc/fluentd/fluentd.conf' \
  -p '/fluentd/plugins' \
  "${FLUENTD_FLAGS[@]}" \
  1>/proc/1/fd/1 2>/proc/1/fd/1 &
fluentd_pid=$!

wait $fluentd_pid
